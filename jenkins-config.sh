#! /usr/bin/bash

scp config.tar jenkins:~
ssh jenkins 'sudo tar -xvf ~/config.tar -C /data/'
ssh jenkins 'sudo systemctl restart jenkins'