
resource "azurerm_virtual_network" "vnet" {
    name                = "mon_vn"
    location            = azurerm_resource_group.rg.location
    resource_group_name = azurerm_resource_group.rg.name
    address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "subnet" {
    name                 = "subnet"
    resource_group_name  = azurerm_resource_group.rg.name
    virtual_network_name = azurerm_virtual_network.vnet.name
    address_prefix       = "10.0.1.0/24"
}

resource "azurerm_network_interface" "nic" {
    count                   = var.machines_number
    name                    = "nic${count.index}"
    location                = azurerm_resource_group.rg.location
    resource_group_name     = azurerm_resource_group.rg.name

    ip_configuration {
        name                          = "internal"
        subnet_id                     = azurerm_subnet.subnet.id
        private_ip_address_allocation = "Static"
        private_ip_address            = var.private_ips[count.index]
        public_ip_address_id          = (count.index == 2 ? azurerm_public_ip.pub_ip.id : null)   
    }
}

resource "azurerm_network_interface_security_group_association" "secu_assoc_jenkins" {
    network_interface_id      = azurerm_network_interface.nic[0].id
    network_security_group_id = azurerm_network_security_group.secu_jenkins.id
}

resource "azurerm_network_interface_security_group_association" "secu_assoc_slave" {
    network_interface_id      = azurerm_network_interface.nic[1].id
    network_security_group_id = azurerm_network_security_group.secu_slave.id
}

resource "azurerm_network_interface_security_group_association" "secu_assoc_reverseproxy" {
    network_interface_id      = azurerm_network_interface.nic[2].id
    network_security_group_id = azurerm_network_security_group.secu_reverseproxy.id
}