// localisation du data center
location            = "northeurope"

// nom du groupe de ressources azure
resource_group_name = "jpr"

// nombre de machines virtuelles créées
machines_number     = 3
// nom de chaque machine respectivement
machines_names      = [
    "jenkins",
    "slave",
    "reverseproxy"
]
// type de chaque machine
machines_types      = [
    "Standard_B1ms",
    "Standard_B2ms",
    "Standard_B1s"
]
// ip privée de chauqe machine
private_ips         = [
    "10.0.1.4",
    "10.0.1.6",
    "10.0.1.5"
]
// nom de l'utilisateur créé sur les machines
user_name           = "jpr"

// domain name sur le cloud azure
// domain_name         = "jpr-devops"

// domain name sur le cloud azure
domain_name         = "jpr-devops-re"

// l'IP de la PIC sera <domain_name>.<location>.cloudapp.azure.com
