#! /usr/bin/bash

cd terraform

terraform init

terraform plan -out plan.out -var "location=northeurope" -var "domain_name=jpr-devops"

terraform apply plan.out

cd ../ansible/

ansible-playbook playbook-all.yml -i inventory.INI --extra-vars "domain=jpr-devops" --extra-vars "location=northeurope"

