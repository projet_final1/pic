# Lire le fichier ProjetFinalDAT.pdf

# Pour rappel, le sujet est décrit dans le fichier Lothaire Projet Final Classe.pdf

# Installation de la Plateforme d'Intégration Continue

## Terraform
Pour utiliser le code terraform, vous devrez créer un fichier secret.auto.tfvars dans le dossier terraform.<br>
Ce fichier devra inclure les lignes suivantes:<br>
```
subscription_id = *votre id de souscription*
client_id       = *votre id de client*
client_secret   = *votre mot de passe*
tenant_id       = *votre id de tenant*
public_key      = *votre clé SSH publique*
```
